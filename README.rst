

=============
FIREFOX UTILS
=============

Collections of scripts and utilities to manage firefox installation on Linux hosts.
The reference target is Debian, but patches for other systems are welcome.



==========
Webography
==========

Some suggestions and references are from the following:

- https://wiki.debian.org/Firefox

